# shellcheck shell=bash

lto_jobs=$((${EXJOBS:-1} / 2))
[[ $lto_jobs -lt 1 ]] && lto_jobs=1

lto_clang_OPT_FLAGS=(-flto=thin)
lto_clang_LDFLAGS=(-flto-jobs="${lto_jobs}")
lto_gcc_OPT_FLAGS=(-flto="${lto_jobs}" -fdevirtualize-at-ltrans)
lto_gcc_fat_OPT_FLAGS=("${lto_gcc_OPT_FLAGS[@]}" -ffat-lto-objects)

lto_OPT_FLAGS=()
lto_LDFLAGS=()
lto_RUSTFLAGS=()

# shellcheck disable=SC2195
case "${CATEGORY}/${PN}" in
# Packages that cannot have LTO enabled
app-crypt/tpm2-tss) ;& # Undefined symbol errors with gcc
app-emulation/wine) ;&
app-office/libreoffice) ;& # Configure test failures
dev-cpp/cppunit) ;& # installs static libraries, tests fail with LTO
dev-cpp/gtest) ;& # Tests fail with LTO
dev-db/postgresql) ;& # gcc CPPFLAGS leak into clang CFLAGS (exherbo env bug)
dev-lang/node) ;& # Linking error with gcc
dev-lang/perl) ;&
dev-lang/python) ;& # Configure test for endianness fails. Installs a static lib. Persists build-time cflags.
dev-lang/ruby) ;& # Tests crash on some ruby versions
dev-lang/rust) ;& # Requires special handling for rust compiler bootstrap
dev-libs/gnutls) ;& # Compiler issue? Hangs in gcc lto-wpa on certtool
dev-libs/libcbor) ;& # fuzz test hangs
dev-libs/pcre2) ;& # Installs static libraries, Concerns about possibly failing tests
dev-libs/spidermonkey) ;& # LTO managed by exheres
dev-python/*) ;& # Doesn't use CFLAGS from build env anyways, uses flags persisted from python build.
dev-util/strace) ;& # Configure checks fail due to CFLAGS in CPPFLAGS
gnome-bindings/gjs) ;& # generates general protection fault when starting up gnome
graphics/exiv2) ;& # Tests crash
media/ffmpeg) ;& # Incompatible relocations in PIC objects
media-libs/dav1d) ;& # Starting with GCC 11.1.0, various undefined reference errors during linking
media-video/mkvtoolnix) ;& # Tests fail
media-libs/openexr) ;& # Tests fail
net-misc/openssh) ;& # hangs on exit with lto
net-www/firefox) ;& # Internal LTO
sys-apps/fwupd) ;& # Runtime failures
sys-apps/paludis) ;& # Test and runtime failures
sys-boot/gnu-efi) ;& # Installs static libraries - intentionally, with special compile options.
sys-devel/gdb) ;&
sys-fs/btrfs-progs) ;& # Installs static libraries
sys-libs/libcap) ;& # Installs static libraries
sys-libs/libseccomp) ;& # Installs static libraries
sys-libs/libxcrypt) ;& # Undefined symbols in library files cause dependencies like net-misc/whois to fail to build
sys-libs/ncurses) ;& # static libs
x11-dri/mesa) ;& # Mesa is broken with LTO, needs upstream fixes
x11-libs/qtbase) ;& # Assembler error in GCC
x11-server/xorg-server) ;& # Some warnings caught with -Werror that only hit with LTO enabled
_LTO_DISABLED) ;;

# Packages which work with LTO but require -ffat-lto-objects (enable LTO on gcc only, clang doesn't support fat objects)
app-arch/brotli) ;& # Installs static libraries
app-arch/bzip2) ;& # Installs static libraries
app-arch/lz4) ;& # Installs static libraries
dev-lang/spirv-tools) ;& # Installs static libraries
dev-libs/libgpg-error) ;& # Installs static libraries
dev-libs/mpfr) ;& # Installs static libraries
dev-libs/nss) ;& # Installs static libraries
dev-libs/xxHash) ;& # installs static libraries
dev-util/elfutils) ;& # Installs static libraries
media-libs/SDL_image) ;& # static libs
media-libs/aom) ;& # static libs
sys-devel/bison) ;& # Installs static libraries
sys-libs/db) ;& # Installs static libraries
x11-libs/libXi) ;& # static libs
_LTO_FAT_OBJECTS)
	case "${brc_cc_PROVIDER:?}" in
	gcc)
		lto_OPT_FLAGS=("${lto_gcc_fat_OPT_FLAGS[@]}")
		;;
	esac
	;;

# Packages for which LTO is allowed
app-arch/gzip) ;&
app-arch/libarchive) ;&
app-arch/libjcat) ;&
app-arch/pigz) ;&
app-arch/unrar) ;&
app-arch/xz) ;&
app-arch/zstd) ;&
app-crypt/gnupg) ;&
app-crypt/krb5) ;&
app-crypt/pinentry) ;&
app-crypt/rhash) ;&
app-doc/doxygen) ;&
app-editors/gedit) ;&
app-editors/gvim) ;&
app-editors/vim) ;&
app-pim/tracker) ;&
app-pim/tracker-miners) ;&
app-shells/bash) ;&
app-text/cmark) ;&
app-text/ghostscript) ;&
app-text/poppler) ;&
app-text/qpdf) ;&
app-virtualization/qemu) ;&
dev-db/ldb) ;&
dev-db/postgresql-client) ;&
dev-db/sqlite) ;&
dev-db/tdb) ;&
dev-lang/python) ;&
dev-lang/vala) ;&
dev-libs/at-spi2-core) ;&
dev-libs/boehm-gc) ;&
dev-libs/boost) ;&
dev-libs/fmt) ;&
dev-libs/fribidi) ;&
dev-libs/gexiv2) ;&
dev-libs/glib) ;&
dev-libs/glib-networking) ;&
dev-libs/highway) ;&
dev-libs/icu) ;&
dev-libs/jsoncpp) ;&
dev-libs/libatomic_ops) ;&
dev-libs/libevent) ;&
dev-libs/libfido2) ;&
dev-libs/libglvnd) ;&
dev-libs/libgusb) ;&
dev-libs/libhandy) ;&
dev-libs/libhttpseverywhere) ;&
dev-libs/libksba) ;&
dev-libs/libosinfo) ;&
dev-libs/libmd) ;&
dev-libs/libpeas) ;&
dev-libs/libsecret) ;&
dev-libs/libtasn1) ;&
dev-libs/libxmlb) ;&
dev-libs/ocl-icd) ;&
dev-libs/openssl) ;&
dev-libs/talloc) ;&
dev-libs/tevent) ;&
dev-libs/vte) ;&
dev-rust/ripgrep) ;&
dev-scm/git) ;&
dev-scm/libgit2) ;&
gnome-bindings/pygobject) ;&
gnome-desktop/cheese) ;&
gnome-desktop/colord-gtk) ;&
gnome-desktop/dconf) ;&
gnome-desktop/dconf-editor) ;&
gnome-desktop/eog) ;&
gnome-desktop/evince) ;&
gnome-desktop/evolution-data-server) ;&
gnome-desktop/file-roller) ;&
gnome-desktop/gcr) ;&
gnome-desktop/gdm) ;&
gnome-desktop/gnome-autoar) ;&
gnome-desktop/gnome-bluetooth) ;&
gnome-desktop/gnome-calculator) ;&
gnome-desktop/gnome-calendar) ;&
gnome-desktop/gnome-clocks) ;&
gnome-desktop/gnome-control-center) ;&
gnome-desktop/gnome-desktop) ;&
gnome-desktop/gnome-disk-utility) ;&
gnome-desktop/gnome-font-viewer) ;&
gnome-desktop/gnome-keyring) ;&
gnome-desktop/gnome-maps) ;&
gnome-desktop/gnome-online-accounts) ;&
gnome-desktop/gnome-session) ;&
gnome-desktop/gnome-settings-daemon) ;&
gnome-desktop/gnome-shell) ;&
gnome-desktop/gnome-system-monitor) ;&
gnome-desktop/gnome-terminal) ;&
gnome-desktop/gnome-text-editor) ;&
gnome-desktop/gnome-weather) ;&
gnome-desktop/gobject-introspection) ;&
gnome-desktop/gsound) ;&
gnome-desktop/gspell) ;&
gnome-desktop/gtksourceview) ;&
gnome-desktop/gvfs) ;&
gnome-desktop/libgdata) ;&
gnome-desktop/libgweather) ;&
gnome-desktop/libsoup) ;&
gnome-desktop/mutter) ;&
gnome-desktop/nautilus) ;&
gnome-desktop/seahorse) ;&
gnome-desktop/tepl) ;&
gnome-extra/gucharmap) ;&
gps/geoclue) ;&
kde-frameworks/attica) ;&
kde-frameworks/karchive) ;&
kde-frameworks/kauth) ;&
kde-frameworks/kbookmarks) ;&
kde-frameworks/kcodecs) ;&
kde-frameworks/kcompletion) ;&
kde-frameworks/kconfig) ;&
kde-frameworks/kconfigwidgets) ;&
kde-frameworks/kcoreaddons) ;&
kde-frameworks/kcrash) ;&
kde-frameworks/kdbusaddons) ;&
kde-frameworks/kded) ;&
kde-frameworks/kdoctools) ;&
kde-frameworks/kguiaddons) ;&
kde-frameworks/kglobalaccel) ;&
kde-frameworks/ki18n) ;&
kde-frameworks/kiconthemes) ;&
kde-frameworks/kinit) ;&
kde-frameworks/kio) ;&
kde-frameworks/kirigami) ;&
kde-frameworks/kitemviews) ;&
kde-frameworks/kjobwidgets) ;&
kde-frameworks/knewstuff) ;&
kde-frameworks/knotifications) ;&
kde-frameworks/knotifyconfig) ;&
kde-frameworks/kpackage) ;&
kde-frameworks/kparts) ;&
kde-frameworks/kpty) ;&
kde-frameworks/kservice) ;&
kde-frameworks/ktextwidgets) ;&
kde-frameworks/kwallet) ;&
kde-frameworks/kwidgetsaddons) ;&
kde-frameworks/kwindowsystem) ;&
kde-frameworks/kxmlgui) ;&
kde-frameworks/solid) ;&
kde-frameworks/sonnet) ;&
mail-client/evolution) ;&
mail-client/geary) ;&
measurement/argyllcms) ;&
media/mpv) ;&
media/pipewire) ;&
media/wireplumber) ;&
media-gfx/ImageMagick) ;&
media-gfx/blender) ;&
media-gfx/darktable) ;&
media-gfx/enblend-enfuse) ;&
media-gfx/gimp) ;&
media-gfx/graphviz) ;&
media-gfx/hugin) ;&
media-gfx/inkscape) ;&
media-gfx/shotwell) ;&
media-gfx/xsane) ;&
media-libs/OpenJPEG) ;&
media-libs/SVT-AV1) ;&
media-libs/babl) ;&
media-libs/freetype) ;&
media-libs/gd) ;&
media-libs/gegl) ;&
media-libs/imath) ;&
media-libs/jasper) ;&
media-libs/lib2geom) ;&
media-libs/libass) ;&
media-libs/libavif) ;&
media-libs/libbluray) ;&
media-libs/libde265) ;&
media-libs/libebml) ;&
media-libs/libheif) ;&
media-libs/libjxl) ;&
media-libs/libmatroska) ;&
media-libs/libmediaart) ;&
media-libs/libmtp) ;&
media-libs/libmypaint) ;&
media-libs/libsamplerate) ;&
media-libs/libudfread) ;&
media-libs/libuninameslist) ;&
media-libs/libwebp) ;&
media-libs/opusfile) ;&
media-libs/rubberband) ;&
media-libs/soundtouch) ;&
media-libs/soxr) ;&
media-libs/tiff) ;&
media-libs/v4l-utils) ;&
media-libs/webrtc-audio-processing) ;&
media-libs/x265) ;&
media-plugins/gst-plugins-bad) ;&
media-plugins/gstreamer-vaapi) ;&
media-sound/pulseaudio) ;&
media-sound/sox) ;&
media-sound/wavpack) ;&
net-analyzer/wireshark) ;&
net-apps/NetworkManager) ;&
net-directory/openldap) ;&
net-dns/c-ares) ;&
net-dns/dnsmasq) ;&
net-dns/bind-tools) ;&
net-firewall/nftables) ;&
net-fs/samba) ;&
net-im/libnice) ;&
net-libs/cyrus-sasl) ;&
net-libs/libmbim) ;&
net-libs/libnma) ;&
net-libs/libqmi) ;&
net-libs/libssh2) ;&
net-libs/libtirpc) ;&
net-libs/nghttp2) ;&
net-libs/webkit) ;&
net-misc/curl) ;&
net-misc/neon) ;&
net-misc/rsync) ;&
net-misc/wget) ;&
net-print/cups) ;&
net-scanner/nmap) ;&
net-utils/gmime) ;&
net-wireless/bluez) ;&
net-wireless/wpa_supplicant) ;&
net-www/epiphany) ;&
net-www/lynx) ;&
office-libs/libical) ;&
sci-libs/fftw) ;&
sci-libs/gsl) ;&
sys-apps/accountsservice) ;&
sys-apps/busybox) ;&
sys-apps/colord) ;&
sys-apps/coreutils) ;&
sys-apps/dbus) ;&
sys-apps/debianutils) ;&
sys-apps/flatpak) ;&
sys-apps/gawk) ;&
sys-apps/iproute2) ;&
sys-apps/kmod) ;&
sys-apps/systemd) ;&
sys-apps/udisks) ;&
sys-apps/util-linux) ;&
sys-apps/vulkan-tools) ;&
sys-apps/xdg-desktop-portal) ;&
sys-devel/cmake) ;&
sys-devel/gettext) ;&
sys-devel/libostree) ;&
sys-devel/make) ;&
sys-devel/mold) ;&
sys-fs/cryptsetup) ;&
sys-fs/e2fsprogs) ;&
sys-fs/exfatprogs) ;&
sys-fs/fuse) ;&
sys-libs/egl-wayland) ;&
sys-libs/gdbm) ;&
sys-libs/libcap) ;&
sys-libs/libinput) ;&
sys-libs/libseccomp) ;&
sys-libs/vulkan-loader) ;&
sys-power/thermald) ;&
sys-process/atop) ;&
sys-process/htop) ;&
text-libs/libstemmer) ;&
x11-apps/libva-utils) ;&
x11-apps/xterm) ;&
x11-dri/libdrm) ;&
x11-drivers/xf86-input-libinput) ;&
x11-drivers/xf86-video-dummy) ;&
x11-libs/gdk-pixbuf) ;&
x11-libs/gtk) ;&
x11-libs/gtk+) ;&
x11-libs/graphene) ;&
x11-libs/harfbuzz) ;&
x11-libs/libSM) ;&
x11-libs/libX11) ;&
x11-libs/libXaw) ;&
x11-libs/libXft) ;&
x11-libs/libXres) ;&
x11-libs/libXtst) ;&
x11-libs/libXxf86vm) ;&
x11-libs/libchamplain) ;&
x11-libs/libva) ;&
x11-libs/libxcvt) ;&
x11-libs/libxkbcommon) ;&
x11-libs/libxklavier) ;&
x11-libs/qtsvg) ;&
x11-libs/webp-pixbuf-loader) ;&
x11-server/xwayland) ;&
_LTO_ENABLED)
	case "$brc_cc_PROVIDER" in
	clang)
		lto_OPT_FLAGS=("${lto_clang_OPT_FLAGS[@]}")
		lto_LDFLAGS=("${lto_clang_LDFLAGS[@]}")
		;;
	gcc)
		lto_OPT_FLAGS=("${lto_gcc_OPT_FLAGS[@]}")
		;;
	esac
	export CARGO_PROFILE_RELEASE_LTO=thin
	lto_RUSTFLAGS=(-Clinker="${brc_TOOL_PREFIX}"clang -Clink-arg=-fuse-ld=lld -Clink-arg=-flto-jobs="${lto_jobs}")
	;;
esac

brc_OPT_FLAGS+=("${lto_OPT_FLAGS[@]}")
brc_LDFLAGS+=("${lto_LDFLAGS[@]}")
brc_RUSTFLAGS+=("${lto_RUSTFLAGS[@]}")

unset lto_OPT_FLAGS
unset lto_LDFLAGS
unset lto_RUSTFLAGS

unset lto_jobs
unset lto_clang_OPT_FLAGS
unset lto_clang_LDFLAGS
unset lto_gcc_OPT_FLAGS
unset lto_gcc_fat_OPT_FLAGS
