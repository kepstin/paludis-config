# shellcheck shell=bash disable=SC2034

if [[
	(
		-n ${BRC_GCC_TOOLCHAIN} ||
		${brc_cc_PROVIDER:?} = gcc && ${brc_gcc_PROVIDER:?} -ge 11 ||
		${brc_cc_PROVIDER} = clang && ${brc_clang_PROVIDER:?} -ge 12
	) &&
	(
		-z ${BRC_GCC_TOOLCHAIN} || ${SLOT} -ge 11
	)
]]
then
	brc_x86_64_CPU_FLAGS=(-march=x86-64-v3)
	brc_i686_CPU_FLAGS=(-march=x86-64-v3)
else
	brc_x86_64_CPU_FLAGS+=(-mavx -mavx2 -mbmi -mbmi2 -mf16c -mfma -mlzcnt -mmovbe -mxsave)
	brc_i686_CPU_FLAGS+=(-mavx -mavx2 -mbmi -mbmi2 -mf16c -mfma -mlzcnt -mmovbe -mxsave)
fi

brc_RUSTFLAGS=(-Ctarget-cpu=x86-64-v3)
export GOAMD64=v3
