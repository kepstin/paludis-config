# shellcheck shell=bash disable=SC2034

# Based on gentooLTO workarounds: https://github.com/InBetweenNames/gentooLTO/blob/master/sys-config/ltoize/files/package.cflags/optimizations.conf

# shellcheck disable=SC2195
case "${CATEGORY}/${PN}" in

# Workaround: Do not apply -O3 (leave as -O2)
kde-frameworks/kactivities-stats) ;& # causes systemsettings5 to crash
mail-filter/procmail) ;& # Causes compile to hang indefinitely
media-libs/faad2) ;& # causes subtly wrong decoding
media-libs/lcms2) ;& # Test failure
net-misc/dhcp) ;& # Runtime failure, DHCPDISCOVER doesn't work correctly - introduced with gcc 10?
sys-apps/systemd) ;& # causes homectl to fail with protocol error
_) ;;

# Default: Apply -O3 to all remaining packages
*)
	brc_OPT_FLAGS=(-O3)
	export CARGO_PROFILE_RELEASE_OPT_LEVEL=3
	;;
esac

