# shellcheck shell=bash disable=SC2034

if [[
	(
		-n ${BRC_GCC_TOOLCHAIN} ||
                ${brc_cc_PROVIDER:?} = gcc && ${brc_gcc_PROVIDER:?} -ge 11 ||
                ${brc_cc_PROVIDER} = clang && ${brc_clang_PROVIDER:?} -ge 12
	) &&
	(
		-z ${BRC_GCC_TOOLCHAIN} || ${SLOT} -ge 11
	)
]]
then
	brc_x86_64_CPU_FLAGS=(-march=x86-64-v2)
	brc_i686_CPU_FLAGS=(-march=x86-64-v2)
else
	brc_x86_64_CPU_FLAGS+=(-mcx16 -msahf -mpopcnt -msse3 -msse4.1 -msse4.2 -mssse3)
	brc_i686_CPU_FLAGS+=(-mcx16 -msahf -mpopcnt -msse3 -msse4.1 -msse4.2 -mssse3)
fi

brc_RUSTFLAGS=(-Ctarget-cpu=x86-64-v2)
export GOAMD64=v2
