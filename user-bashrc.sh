# shellcheck shell=bash

# Environment variables and helpful aliases for user sessions
# To use: Either source this file from your user's bashrc, or consider
# symlinking it into /etc/bash/bashrc.d so it's loaded system-wide.

export CAVE_RESOLVE_OPTIONS="--recommendations display --continue-on-failure if-satisfied --purge */*"
export CAVE_RESUME_OPTIONS="--continue-on-failure if-satisfied"
# cave search index is updated by hooks/auto/cave-search-index.hook
export CAVE_SEARCH_OPTIONS="--index /var/cache/paludis/cave-search-index"

# "eclectic config interactive" is annoyingly long to type
[[ ${EUID} -eq 0 ]] && alias eci="eclectic config interactive"

# A helper wrapper for cave that manages automatically setting up a per-session resume file.
cave() {
	declare -g PALUDIS_RESUME_FILE
	[[ -z ${PALUDIS_RESUME_FILE} || ! -f ${PALUDIS_RESUME_FILE} ]] && PALUDIS_RESUME_FILE="$(mktemp --tmpdir paludis-resume.XXXXXXXXXX)"
	local PALUDIS_RESUME_OPT
	[[ -n ${PALUDIS_RESUME_FILE} ]] && PALUDIS_RESUME_OPT="--resume-file ${PALUDIS_RESUME_FILE}"

	# Add the resume file option without modifying the global value of any options vars
	declare -x CAVE_RESOLVE_OPTIONS="${PALUDIS_RESUME_OPT} ${CAVE_RESOLVE_OPTIONS}"
	declare -x CAVE_UNINSTALL_OPTIONS="${PALUDIS_RESUME_OPT} ${CAVE_UNINSTALL_OPTIONS}"
	declare -x CAVE_RESUME_OPTIONS="${PALUDIS_RESUME_OPT} ${CAVE_RESUME_OPTIONS}"

	# Total system memory in megabytes
	declare mem_total
	mem_total=$(awk '$1~/^MemTotal:/{printf "%d\n", $2 / 1024}' /proc/meminfo)

	case "$1" in
	fix-linkage|resolve|resume|sync)
		systemd-run \
			--property=CPUWeight=idle \
			--property=IOWeight=1 \
			--property=MemoryHigh=$((mem_total / 2))M \
			--property=MemoryMax=$((mem_total * 3 / 4))M \
			--property=ManagedOOMMemoryPressure=kill \
			--working-directory=/ \
			--setenv=HOME="${HOME}" \
			--setenv=CAVE_RESOLVE_OPTIONS="${CAVE_RESOLVE_OPTIONS}" \
			--setenv=CAVE_UNINSTALL_OPTIONS="${CAVE_UNINSTALL_OPTIONS}" \
			--setenv=CAVE_RESUME_OPTIONS="${CAVE_RESUME_OPTIONS}" \
			--setenv=CAVE_SEARCH_OPTIONS="${CAVE_SEARCH_OPTIONS}" \
			--setenv=PALUDIS_DO_NOTHING_SANDBOXY="${PALUDIS_DO_NOTHING_SANDBOXY}" \
			--pipe --pty --wait --collect --user \
			cave "${@}"
		;;
	*)
		command cave "${@}"
		;;
	esac
}
