#!/bin/bash

hook_run_ebuild_postinst_post() {
    # Calling hooked echo functions in a hook overwrites the HOOK variable
    local HOOK=${HOOK}

    [[ ${CATEGORY}/${PN} = sys-libs/glibc ]] || return

    local LOCALES
    local localedef="${ROOT}"usr/$(exhost --target)/bin/localedef
    local locale_gen

    eval "$(source /etc/paludis/hooks/config/locale-gen.conf ; declare -p LOCALES)"

    einfo "locale-gen: Compiling ${#LOCALES[@]} additional locale(s)"

    for locale_gen in "${LOCALES[@]}" ; do
	    local locale lang codeset modifier name charmap input 
	    locale=${locale_gen% *}
	    name=${locale}
	    charmap=${locale_gen#"${locale}"}
	    charmap=${charmap# }

	    # Locale name format is language[_territory[.codeset]][@modifier]
	    # to get the input name for localedef, the codeset has to be removed
	    # from the middle
	    lang=${locale%.*}
	    lang=${lang%@*}
	    locale=${locale#"${lang}"}
	    locale=${locale#.}
	    codeset=${locale%@*}
	    locale=${locale#"${codeset}"}
	    locale=${locale#@}
	    modifier=${locale}

	    input=${lang}
	    [[ -n ${modifier} ]] && input+="@${modifier}"

	    if [[ -z ${name} || -z ${input} || -z ${charmap} ]]; then
		    ewarn "locale-gen: failed to parse locale ${locale@Q}"
		    continue
	    fi

	    I18NPATH="${ROOT}"usr/share/i18n nonfatal edo \
		    "${localedef}" --prefix="${ROOT}" -i "${input}" -f "${charmap}" "${name}"
    done
}

[[ ${HOOK} = ebuild_postinst_post ]] && hook_run_ebuild_postinst_post

unset hook_run_ebuild_postinst_post
