#!/bin/bash

hook_run_ebuild_prepare_pre() {
    # Calling hooked echo functions in a hook overwrites the HOOK variable
    local HOOK=${HOOK}
    local configdir=/etc/paludis/hooks/config/userpatch

    # A few build systems which do out of tree builds set WORK to the build
    # directory, but we need to be in the source directory to apply patches.
    local saved_pwd=${PWD}
    local work=${WORK}
    [[ -n ${CMAKE_SOURCE} ]] && work=${CMAKE_SOURCE}
    [[ -n ${MESON_SOURCE} ]] && work=${MESON_SOURCE}
    cd "${work}" || die "cd to ${work@Q} failed"

    local -a patchdirs=("${PN}" "${PNV}")
    [[ $PNVR != "$PNV" ]] && patchdirs+=( "${PNVR}" )

    local patchdir
    for patchdir in "${patchdirs[@]}" ; do
	    local fullpatchdir="${configdir}/${CATEGORY}/${patchdir}"
	    [[ -d ${fullpatchdir} ]] || continue
	    einfo "userpatch: Applying patches from ${CATEGORY}/${patchdir} in ${PWD}"
	    expatch "${fullpatchdir}"
    done
    cd "${saved_pwd}" || die "restoring saved pwd ${saved_pwd@Q} failed"
}

[[ ${HOOK} = ebuild_prepare_pre ]] && hook_run_ebuild_prepare_pre

unset hook_run_ebuild_prepare_pre
