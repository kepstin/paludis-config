#!/bin/bash

hook_run_ebuild_install_post() {
    # Calling hooked echo functions in a hook overwrites the HOOK variable
    local HOOK=${HOOK}

    # Ensure modified shopt config gets restored
    local -r saved_shopt=$(shopt -p nullglob globstar)
    trap 'eval "${saved_shopt}"' RETURN
    shopt -s nullglob globstar

    local keep_reason
    if declare -pF toolchain-runtime-libraries_pkg_preinst >/dev/null 2>&1 ; then
	    keep_reason="toolchain-runtime-libraries.exlib alternatives break"
    fi

    case "${CATEGORY}/${PN}" in
    sys-devel/libtool) keep_reason="libtool doesn't like having its own .la files removed" ;;
    media-gfx/ImageMagick) keep_reason="uses libltdl to load image modules" ;;
    app-text/opensp) keep_reason="openjade links to this by the installed .la file"
    esac

    if [[ -n $keep_reason ]]; then
	    einfo "lafilecleaner: Keeping .la files for ${CATEGORY}/${PN}: ${keep_reason}"
	    return
    fi

    local lafile
    for lafile in "${IMAGE}"/usr/*/lib/**/*.la "${IMAGE}"/etc/env.d/alternatives/*/*/usr/*/lib/**/*.la
    do
	    local lafile_short="${lafile#"${IMAGE}"}"
	    [[ -e ${lafile%.la}.a ]] && ewarn "lafilecleaner: ${lafile_short}: static library present"
	    [[ ! -e ${lafile%.la}.so ]] && ewarn "lafilecleaner: ${lafile_short}: no corresponding .so"

	    einfo "lafilecleaner: Removing ${lafile_short}"
	    rm "${lafile}"
	    rmdir -p --ignore-fail-on-non-empty "${lafile%/*}"
    done
}

[[ ${HOOK} = ebuild_install_post ]] && hook_run_ebuild_install_post

unset hook_run_ebuild_install_post
