#!/bin/bash

hook_run_ebuild_install_post() {
    # Calling hooked echo functions in a hook overwrites the HOOK variable
    local HOOK=${HOOK}
    local target=$(exhost --target)

    [[ -d "${IMAGE}/usr/${target}/share/gir-1.0" ]] \
        && die "Package created gir-1.0 dir in /usr/${target}/share"
    [[ -d "${IMAGE}/usr/${target}/${target}/lib/girepository-1.0" ]] \
        && die "Package created girepository-1.0 dir in /usr/${target}/${target}/lib"
    [[ -d "${IMAGE}/usr/lib/girepository-1.0" ]] \
        && die "Package created girepository-1.0 dir in /usr/lib"
}

[[ ${HOOK} = ebuild_install_post ]] && hook_run_ebuild_install_post

unset hook_run_ebuild_install_post
