# shellcheck shell=bash

case "$(uname -m)" in
x86_64|i686)
	brc_x86_64_CPU_FLAGS+=(-mtune=native)
	brc_i686_CPU_FLAGS+=(-mtune=native)
	# TODO: Add -Ctune-cpu to RUSTFLAGS when supported
	;;
aarch64)
	# TODO: determine big.LITTLE somehow?
	cpu_model="$(awk -F ': ' '$1~/^CPU part\s*$/{print $2 ; exit}' /proc/cpuinfo)"
	if [[ $cpu_model = 0xd03 ]]; then
		tune=cortex-a53
	fi
	[[ -n $tune ]] && brc_aarch64_CPU_FLAGS+=(-mtune="$tune")
	unset cpu_model
	unset tune
	;;
esac
