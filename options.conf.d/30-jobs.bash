#!/bin/bash

declare -g nproc mem_total
# Number of processing units available to the current process
nproc=$(nproc)
# Total system memory, in megabytes
mem_total=$(awk '$1~/^MemTotal:/{printf "%d\n", $2 / 1024}' /proc/meminfo)

# proc_limit PROC [MAX_PROC]
# Limit number of processes PROC to minimum of 1 and maximum of MAX_PROC, or
# $nproc if MAX_PROC is not specified
proc_limit() {
	declare proc=$1 max_proc=${2:-$nproc}
	echo $((proc < 1 ? 1 : proc > max_proc ? max_proc : proc))
}

# proc_per_mem PROC_MEM
# Determine number or processes to use assuming each requires PROC_MEM megabytes
proc_per_mem() {
	declare proc_mem=$1
	proc_limit $((mem_total / proc_mem))
}

declare -g proc2 proc4 proc8 proc_samba
proc2=$(proc_per_mem 1920)
proc4=$(proc_per_mem 3840)
proc8=$(proc_per_mem 7680)
proc_samba=$(proc_limit "$proc2" 8)

cat <<END
*/* build_options: jobs=$proc2
dev-lang/node build_options: jobs=$proc4
dev-libs/spidermonkey build_options: jobs=$proc4
net-fs/samba build_options: jobs=$proc_samba
net-libs/webkit build_options: jobs=$proc4
net-www/chromium-stable build_options: jobs=$proc4
net-www/firefox build_options: jobs=$proc4
x11-libs/qtwebengine build_options: jobs=$proc8
END
