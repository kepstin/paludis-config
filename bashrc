# shellcheck shell=bash disable=SC2034

brc_x86_64_CPU_FLAGS=(-march=x86-64)
brc_x86_64_OPT_FLAGS=(-fno-omit-frame-pointer)
brc_i686_CPU_FLAGS=(-march=x86-64)
brc_i686_OPT_FLAGS=()
brc_aarch64_CPU_FLAGS=(-march=armv8-a)
brc_aarch64_OPT_FLAGS=()
brc_OPT_FLAGS=(-O2)
brc_LDFLAGS=('-Wl,-O1')
brc_RUSTFLAGS=()

brc_cc_PROVIDER=$(eclectic cc show)
brc_c___PROVIDER=$(eclectic c++ show)
brc_cpp_PROVIDER=$(eclectic cpp show)
brc_gcc_PROVIDER=$(eclectic gcc show)
brc_clang_PROVIDER=$(eclectic clang show)
brc_TOOL_PREFIX="$(type exhost &>/dev/null && exhost --tool-prefix)"
case "${CATEGORY}/${PN}" in
sys-devel/gcc|sys-libs/libatomic|sys-libs/libgcc|sys-libs/libgfortran|sys-libs/libgomp|sys-libs/libsanitizer|sys-libs/libstdc++|sys-libs/libquadmath) BRC_GCC_TOOLCHAIN=yes ;;
esac

[[ ${brc_cc_PROVIDER} = "${brc_c___PROVIDER}" ]] || die "Eclectic cc and c++ providers don't match"
[[ ${brc_cc_PROVIDER} = "${brc_cpp_PROVIDER}" ]] || die "Eclectic cc and cpp providers don't match"

for bashrc_file in "${PALUDIS_CONFIG_DIR}"bashrc.d/* ; do
	# shellcheck source=/dev/null
	[[ -e ${bashrc_file} ]] && source "${bashrc_file}"
done
unset bashrc_file

unset BRC_GCC_TOOLCHAIN
unset brc_cc_PROVIDER
unset brc_c___PROVIDER
unset brc_gcc_PROVIDER
unset brc_clang_PROVIDER
unset brc_TOOL_PREFIX

brc_x86_64_FLAGS=("${brc_x86_64_CPU_FLAGS[@]}" "${brc_OPT_FLAGS[@]}" "${brc_x86_64_OPT_FLAGS[@]}")
x86_64_pc_linux_gnu_CFLAGS="${brc_x86_64_FLAGS[*]}"
x86_64_pc_linux_gnu_CXXFLAGS="${brc_x86_64_FLAGS[*]}"
x86_64_pc_linux_gnu_FCFLAGS="${brc_x86_64_FLAGS[*]}"
x86_64_pc_linux_gnu_FFLAGS="${brc_x86_64_FLAGS[*]}"
x86_64_pc_linux_musl_CFLAGS="${brc_x86_64_FLAGS[*]}"
x86_64_pc_linux_musl_CXXFLAGS="${brc_x86_64_FLAGS[*]}"
x86_64_pc_linux_musl_FCFLAGS="${brc_x86_64_FLAGS[*]}"
x86_64_pc_linux_musl_FFLAGS="${brc_x86_64_FLAGS[*]}"
unset brc_x86_64_FLAGS
brc_i686_FLAGS=("${brc_i686_CPU_FLAGS[@]}" "${brc_OPT_FLAGS[@]}" "${brc_i686_OPT_FLAGS[@]}")
i686_pc_linux_gnu_CFLAGS="${brc_i686_FLAGS[*]}"
i686_pc_linux_gnu_CXXFLAGS="${brc_i686_FLAGS[*]}"
i686_pc_linux_gnu_FCFLAGS="${brc_i686_FLAGS[*]}"
i686_pc_linux_gnu_FFLAGS="${brc_i686_FLAGS[*]}"
unset brc_i686_FLAGS
brc_aarch64_FLAGS=("${brc_aarch64_CPU_FLAGS[@]}" "${brc_OPT_FLAGS[@]}" "${brc_aarch64_OPT_FLAGS[@]}")
aarch64_unknown_linux_gnueabi_CFLAGS="${brc_aarch64_FLAGS[*]}"
aarch64_unknown_linux_gnueabi_CXXFLAGS="${brc_aarch64_FLAGS[*]}"
aarch64_unknown_linux_gnueabi_FCFLAGS="${brc_aarch64_FLAGS[*]}"
aarch64_unknown_linux_gnueabi_FFLAGS="${brc_aarch64_FLAGS[*]}"
unset brc_aarch64_FLAGS

x86_64_pc_linux_gnu_LDFLAGS="${brc_LDFLAGS[*]}"
x86_64_pc_linux_musl_LDFLAGS="${brc_LDFLAGS[*]}"
i686_pc_linux_gnu_LDFLAGS="${brc_LDFLAGS[*]}"
aarch64_unknown_linux_gnueabi_LDFLAGS="${brc_LDFLAGS[*]}"

RUSTFLAGS="${brc_RUSTFLAGS[*]}"
export RUSTFLAGS
# Print linker command and stderr
export RUSTC_LOG=rustc_codegen_ssa::back::link=info

unset brc_x86_64_CPU_FLAGS
unset brc_x86_64_OPT_FLAGS
unset brc_i686_CPU_FLAGS
unset brc_i686_OPT_FLAGS
unset brc_aarch64_CPU_FLAGS
unset brc_aarch64_OPT_FLAGS
unset brc_OPT_FLAGS
unset brc_LDFLAGS
unset brc_RUSTFLAGS
